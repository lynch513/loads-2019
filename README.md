# loads-2019

A Clojure and ClojureScript util designed to ... calculate loads on city power electric stations. This software is used by the employee of electric grid company of Saint-Petersburg, Russia. This is expiremental software and used open source data. If you have any question please e-mail me.  

## Usage

* Download and install Java (https://www.java.com/en/) or install OpenJDK (https://openjdk.java.net/)
* Install Clojure Leiningen (https://leiningen.org/)
* Clone this repo at some local directory. Go into it. Edit or put your data at resources/data.edn using inline dsl and run: 

```console
~$ lein run data.edn
```

## License

Copyright � 2019 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
