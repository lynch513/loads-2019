(defproject loads-2019 "0.4.2-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.10.0"]]

  :resource-paths ["resources"]

  :main ^:skip-aot loads-2019.core

  :target-path "target/%s"

  :profiles 
  {:uberjar
   {:aot :all}

   :dev 
   {:dependencies []
    :source-paths ["dev"]}}
  )
