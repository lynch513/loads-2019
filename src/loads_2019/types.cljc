(ns loads-2019.types
  (:require [clojure.spec.alpha :as s]))


;;------------------------------------------------------------------------------
;; Common types
;;------------------------------------------------------------------------------

(s/def ::name string?)

;;------------------------------------------------------------------------------
;; Lines
;;------------------------------------------------------------------------------

(s/def ::samples (s/coll-of number?))

(s/def ::line
  (s/keys :req-un [::name ::samples]))

(defrecord Line [name samples])

(defn line? [i]
  (s/valid? ::line i))

(defn mk-line [name & samples]
  (let [line (->Line name samples)]
    (if-let [explain (s/explain-data ::line line)]
      (throw (ex-info "Validation error" {:msg     (str "on line " name)
                                          :explain explain}))
      line)))

;;------------------------------------------------------------------------------
;; Sections
;;------------------------------------------------------------------------------

(s/def ::lines
  (s/coll-of ::line))

(s/def ::section
  (s/keys :req-un [::lines]))

(defrecord Section [lines])

(defn section? [i]
  (s/valid? ::section i))

(defn mk-section [& lines]
  (let [section (->Section lines)]
    (if-let [explain (s/explain-data ::section section)]
      (throw (ex-info "Validation error" {:msg     "on section"
                                          :explain explain}))
      section)))

;;------------------------------------------------------------------------------
;; Stations
;;------------------------------------------------------------------------------

(s/def ::sections
  (s/coll-of ::section))

(s/def ::station
  (s/keys :req-un [::name ::sections]))

(defn station? [i]
  (s/valid? ::station i))

(defrecord Station [name sections])

(defn mk-station [name & sections]
  (let [station (->Station name sections)]
    (if-let [explain (s/explain-data ::station station)]
      (throw (ex-info "Validation error" {:msg     (format "on station %s" name)
                                          :explain explain}))
      station)))

;;------------------------------------------------------------------------------
;; Predicates 
;;------------------------------------------------------------------------------

;;------------------------------------------------------------------------------
;; Naive tests
;;------------------------------------------------------------------------------

(comment
  (mk-line "A" 1 2 "3")
  (try
    (mk-line 1 2)
    (catch Exception e
      (println e))))

(comment
  (use '[clojure.pprint :as pprint])
  (pprint (mk-section 
            (mk-line "1" 1 2)
            (mk-line "B" 3 4)))
  (try
    (mk-section 
      (mk-line 1 2) 
      (mk-line "B" 3 4))
    (catch Exception e
      (println e))))

(comment
  (pprint (mk-station "stA"
                     (mk-section 
                       (mk-line "A" 1 2) 
                       (mk-line "B" 3 4))))
  (try
    (mk-station "stA" (mk-section 
                        (mk-line "1" 1 2) 
                        (mk-line "B" 3 4)))
    (catch Exception e
      (println e))))
