(ns loads-2019.edn
  (:require 
    [loads-2019.types :as t]
    #?(:clj [clojure.edn  :as edn]
       :cljs [cljs.reader :as edn])))


;;------------------------------------------------------------------------------
;; Lines
;;------------------------------------------------------------------------------

(defn edn->Line
  "Takes the alternative edn and convert it into a Line"
  [m]
  (t/map->Line m))


;;------------------------------------------------------------------------------
;; Sections
;;------------------------------------------------------------------------------

(defn edn->Section
  "Takes the alternative edn and convert it into a Section"
  [m]
  (t/map->Section m))


;;------------------------------------------------------------------------------
;; Stations
;;------------------------------------------------------------------------------

(defn edn->Station
  "Takes the alternative edn and convert it into a Station"
  [m]
  (t/map->Station m))

;;------------------------------------------------------------------------------
;; Read from edn string
;;------------------------------------------------------------------------------

(defn read-string-with-types
  "Gets string with edn and returns parsed structures with Line, Section, Station.
  It's raw unchecked data and it need to be validate."
  [edn-string]
  (edn/read-string
    {:readers {'loads_2019.types.Line    edn->Line
               'loads_2019.types.Section edn->Section
               'loads_2019.types.Station edn->Station}}
    edn-string))


