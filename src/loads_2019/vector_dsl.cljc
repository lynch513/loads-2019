(ns loads-2019.vector-dsl
  (:require 
    [clojure.spec.alpha :as s]
    #?(:clj  [clojure.edn :as edn]
       :cljs [cljs.reader :as edn])
    [clojure.walk :refer [prewalk]]
    [loads-2019.types :as t #?@(:cljs [:include-macros true])]))


;; (ns-unalias (find-ns 'loads-2019.vector-dsl) 's)

;;------------------------------------------------------------------------------
;; Types
;;------------------------------------------------------------------------------

(s/def ::line 
  (s/cat :id      (s/and keyword? #{:line}) 
         :name    string? 
         :samples (s/+ number?)))

(s/def ::section
  (s/cat :id    (s/and keyword? #{:section})
         :lines (s/+ (s/spec ::line))))

(s/def ::station
  (s/cat :id       (s/and keyword? #{:station})
         :name     string?
         :sections (s/+ (s/spec ::section))))

;;------------------------------------------------------------------------------
;; Helper func
;;------------------------------------------------------------------------------

(defn first? [v k]
  (= (first v) k))

;;------------------------------------------------------------------------------
;; Predicates for Line
;;------------------------------------------------------------------------------

(defn- line-id?
  [v]
  (first? v :line))

(defn- line?
  [v]
  (s/valid? ::line v))

;;------------------------------------------------------------------------------
;; Predicates for Section
;;------------------------------------------------------------------------------

(defn- section-id?
  [v]
  (first? v :section))

(defn- section?
  [v]
  (s/valid? ::section v))

;;------------------------------------------------------------------------------
;; Predicates for Station
;;------------------------------------------------------------------------------

(defn- station-id?
  [v]
  (first? v :station))

(defn station?
  [v]
  (s/valid? ::station v))

;;------------------------------------------------------------------------------
;; Utils
;;------------------------------------------------------------------------------

(defn dispath-fun-items [i]
  (cond 
    (line-id? i)    :line
    (section-id? i) :section
    (station-id? i) :station))

(defmulti get-name dispath-fun-items)

(defmethod get-name :line 
  [v]
  (get v 1))

(defmethod get-name :section 
  [_]
  nil)

(defmethod get-name :station 
  [v]
  (get v 1))

(defmulti get-values dispath-fun-items)

(defmethod get-values :line 
  [v]
  (drop 2 v))

(defmethod get-values :section
  [v]
  (drop 1 v))

(defmethod get-values :station 
  [v]
  (drop 2 v))

;;------------------------------------------------------------------------------
;; Walk on vectors
;;------------------------------------------------------------------------------

(defmulti from-vector-dsl dispath-fun-items)

(defmethod from-vector-dsl :line
  [line]
  (apply (partial t/mk-line (get-name line)) (get-values line)))

(defmethod from-vector-dsl :section
  [section]
  (try
    (apply t/mk-section (map from-vector-dsl (get-values section)))
    (catch #?(:clj Exception :cljs ExceptionInfo) e
      (let [explain (:explain (ex-data e))
            msg     (:msg (ex-data e))]
        (throw (ex-info "Validation error" {:msg     (str msg ", on section")
                                            :explain explain}))))))

(defmethod from-vector-dsl :station
  [station]
  (let [name   (get-name station)
        values (get-values station)]
    (try
     (apply (partial t/mk-station name) (map from-vector-dsl values))
     (catch #?(:clj Exception :cljs ExceptionInfo) e
       (let [explain (:explain (ex-data e))
             msg     (:msg (ex-data e))]
         (throw (ex-info "Validation error" {:msg     (str  "on station, " name " " msg)
                                             :explain explain})))))))

(defmethod from-vector-dsl :default
  [i]
  (if (sequential? i)
    (map from-vector-dsl i)
    i))

(defmacro defvector [& xs]
  `(into [] (concat ~@xs)))

(defn to-vector-dsl
  [model]
  (prewalk
    (fn [i]
      (cond
        (t/line? i)          (defvector [:line (:name i)] (:samples i))
        (t/section? i)       (defvector [:section] (:lines i))
        (t/station? i)       (defvector [:station (:name i)] (:sections i))
        :else              i))
    model))
 

;;------------------------------------------------------------------------------
;; Tests
;;------------------------------------------------------------------------------

(comment
  (use '[clojure.pprint :refer [pprint]]))

(comment
  (line? [:line "A" 1 2 3])
  (get-name [:line "A" 1 2 3])
  (get-samples [:line "A" 1 2 3])
  (get-values [:line "A" 1 2 3])
  (get-values [:section [:line "A" 1] [:line "B" 2]])
  (get-values [:station "A" [:section [:line "B" 1 2 3]]])
  (apply (partial t/mk-line (get-name [:line "A" 1 2 3])) (get-samples [:line "A" 1 2 3]))
  (section? [:section [:line "A" 1] [:line "B" 2]])
  (s/explain ::section [:section [:line "A" 1] [:line "B" 2]])
  (get-name [:section [:line "A" 1] [:line "B" 2]])
  (get-lines [:section [:line "A" 1] [:line "B" 2]])
  (station? [:station "A" [:section [:line "A" 1 2 3]]])
  (get-name [:station "A" [:section [:line "B" 1 2 3]]])
  (get-sections [:station "A" [:section [:line "A" 1 2 3]]])
  (get-name [:line "A" 1 2 3])
  (get-name [:section 1 2 3])
  (get-name [:station "A" 1 2 3])
  (from-vector-dsl [:line "A" 1 2])
  (try
    (from-vector-dsl [:line 'A 1 2])
    (catch Exception e
      (println (ex-data e))))
  (from-vector-dsl [:line "B" 1 2])
  (from-vector-dsl [:section [:line "A" 1 2]])
  (try
    (from-vector-dsl [:section "A" [:line "A" 1 2]])
    (catch Exception e
      (println (ex-data e))))
  (try
    (from-vector-dsl [:station "A"
                     [:section 
                      [:line "A" 1 2] 
                      [:line "B" 3 4]]
                     [:section 
                      [:line 'C 5 6] 
                      [:line "D" 7 8]]
                     ])
    (catch Exception e
      (println (ex-data e))))
  (s/explain :loads-2019.types/section (from-vector-dsl 
                                             [:section 
                                              [:line "3456" 1 2] 
                                              [:line "3457" 3 4]]))
  (from-vector-dsl [:line "A" 1 2])
  (from-vector-dsl [:section [:line "A" 1 2]])
  (from-vector-dsl 
    [:station "3600" 
     [:section 
      [:line "3456" 1 2] 
      [:line "3457" 3 4]]])
  (to-vector-dsl (from-vector-dsl 
     [:station "3600" 
      [:section 
       [:line "A" 1 2] 
       [:line "B" 3 4]]]))
  (s/explain :loads-2019.types/station (from-vector-dsl 
                                         [[:station "3600" 
                                           [:section 
                                            [:line "3456" 1 2] 
                                            [:line "3457" 3 4]]]
                                          [:station "A"
                                           [:section
                                            [:line "B" 1]]]]))
  (from-vector-dsl (edn/read-string (slurp "resources/data.edn")))
  (from-vector-dsl (edn/read-string (slurp "resources/data2.edn")))
  (-> (t/def-station "3600" 
        (t/def-section
          (t/def-line "A" 1 2)
          (t/def-line "B" 3 4))
        (t/def-section
          (t/def-line "C" 5 6)
          (t/def-line "D" 7 8)))
      (to-vector-dsl))
  )

