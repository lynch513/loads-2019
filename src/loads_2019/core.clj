(ns loads-2019.core
  (:require 
    [clojure.edn :as edn]
    [clojure.java.io :as io]
    [clojure.pprint :refer [pprint]]
    ;;
    [loads-2019.fake :refer [recalc-load]]
    [loads-2019.calc :refer [recalc-samples]]
    [loads-2019.vector-dsl :refer [from-vector-dsl to-vector-dsl]]))


(defn read-resource-if-exist 
  "Opens and reads given file-name, returning a string. Throw exception
   java.io.FileNotFoundException if file doesn't exist"
  [file-name]
  (if-let [file-resource (io/resource file-name)]
    (slurp file-resource)
    (throw (java.io.FileNotFoundException. (format "File %s not found" file-name)))))


(defn -main [& args]
  {:pre [(= 1 (count args))
         (string? (first args))]}
  (try (->> (read-resource-if-exist (first args))
        (edn/read-string)
        (map from-vector-dsl)
        #_(map (partial s/validate Station))
        (map #(recalc-samples % recalc-load))
        (to-vector-dsl)
        (pprint))
       (catch Exception e
         (let [title (ex-message e)
               msg   (:msg (ex-data e))
               data  (ex-data e)]
           (println (format "%s: %s" title msg))
           (println data)))))

(comment 
  (-main "data.edn"))
